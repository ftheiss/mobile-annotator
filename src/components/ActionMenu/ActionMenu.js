import "./ActionMenu.css";
import NavigateNextIcon from "@material-ui/icons/NavigateNext";
import NavigateBeforeIcon from "@material-ui/icons/NavigateBefore";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import ClearIcon from "@material-ui/icons/Clear";
import DoneIcon from "@material-ui/icons/Done";

function ActionMenu(props) {
  const isPreviousDisabled = props.imageIndex === props.firstIndex;
  function handleClickPrevious(e) {
    props.onImageIndexChange(props.imageIndex - 1);
  }

  const isNextDisabled = props.imageIndex === props.lastIndex;
  function handleClickNext(e) {
    props.onImageIndexChange(props.imageIndex + 1);
  }

  function handleClickAdd(e) {
    props.onDrawModeToggle();
  }

  function handleClickDelete(e) {
    props.onDeleteModeToggle();
  }

  function handleClickClear(e) {
    console.log("clear");
  }

  function handleClickDone(e) {
    console.log("done");
  }

  return (
    <div className="action-menu-container">
      <button
        onClick={handleClickPrevious}
        disabled={isPreviousDisabled}
        className={isPreviousDisabled ? "disabled" : ""}
      >
        <NavigateBeforeIcon className="vertm" />
      </button>
      <button
        onClick={handleClickNext}
        disabled={isNextDisabled}
        className={isNextDisabled ? "disabled" : ""}
      >
        <NavigateNextIcon className="vertm" />
      </button>
      <button
        onClick={handleClickAdd}
        className={props.mode === "drawBB" ? "active" : ""}
      >
        <AddIcon className="vertm" />
      </button>
      <button
        onClick={handleClickDelete}
        className={props.mode === "deleteBB" ? "active" : ""}
      >
        <DeleteIcon className="vertm" />
      </button>
      <button onClick={handleClickClear} className="clear">
        <ClearIcon className="vertm" />
      </button>
      <button onClick={handleClickDone} className="done">
        <DoneIcon className="vertm" />
      </button>
    </div>
  );
}

export default ActionMenu;
