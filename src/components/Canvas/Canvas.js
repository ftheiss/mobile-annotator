import { useEffect, useState } from "react";
import { Layer, Stage } from "react-konva";
import BaseImage from "../BaseImage/BaseImage";

function Canvas(props) {
  const [stageSize, setStageSize] = useState({
    width: props.width,
    height: props.height,
  });

  useEffect(() => {
    const calcHeight = props.width / 1.778;
    if (calcHeight > props.height) {
      setStageSize({ width: props.height * 1.778, height: props.height });
    } else {
      setStageSize({ width: props.width, height: calcHeight });
    }
  }, [props.height, props.width]);

  return (
    <Stage width={stageSize.width} height={stageSize.height}>
      <Layer>
        <BaseImage
          width={stageSize.width}
          height={stageSize.height}
          image={props.images[props.imageIndex]}
        ></BaseImage>
      </Layer>
    </Stage>
  );
}

export default Canvas;
