import { useState, useReducer } from "react";
import "./App.css";
import ActionMenu from "./components/ActionMenu/ActionMenu";
import Canvas from "./components/Canvas/Canvas";
import Description from "./components/Description/Description";
import { useContentRect } from "./hooks/UseContentRect";
import { description, images } from "./MockData";

const NEXT_STATE_GRAPH = {
  drawBB: {
    DELETE_BB: "deleteBB",
    IDLE: "idle",
  },
  deleteBB: {
    DRAW_BB: "drawBB",
    IDLE: "idle",
  },
  idle: {
    DRAW_BB: "drawBB",
    DELETE_BB: "deleteBB",
  },
};

function reducer(state, event) {
  const nextState = NEXT_STATE_GRAPH[state][event];
  return nextState !== undefined ? nextState : state;
}

function App() {
  const [containerCanvasRect, containerCanvasRef] = useContentRect();

  const [imageIndex, setImageIndex] = useState(0);

  function handleImageIndexChange(index) {
    if (index < 0 || index > images.length - 1) {
      return;
    }
    setImageIndex(index);
  }

  const [mode, dispatch] = useReducer(reducer, "idle");

  function handleDrawMode() {
    mode !== "drawBB" ? dispatch("DRAW_BB") : dispatch("IDLE");
  }

  function handleDeleteMode() {
    mode !== "deleteBB" ? dispatch("DELETE_BB") : dispatch("IDLE");
  }

  return (
    <div className="container">
      <div className="head">
        <Description
          title={description.title}
          tasks={description.tasks}
        ></Description>
      </div>
      <div className="body" ref={containerCanvasRef}>
        {containerCanvasRect !== null && (
          <Canvas
            width={containerCanvasRect.width}
            height={containerCanvasRect.height}
            images={images}
            imageIndex={imageIndex}
          ></Canvas>
        )}
      </div>
      <div className="foot">
        <ActionMenu
          onImageIndexChange={handleImageIndexChange}
          imageIndex={imageIndex}
          firstIndex={0}
          lastIndex={images.length - 1}
          onDrawModeToggle={handleDrawMode}
          onDeleteModeToggle={handleDeleteMode}
          mode={mode}
        ></ActionMenu>
      </div>
    </div>
  );
}

export default App;
