# Mobile Annotator

Mobile annotator is an annotation tool for drawing 2D bounding boxes. The user interface is web-based (react.js) and Konva (react-konva) is used for drawing. The tool is part of an internship where the task was to optimize the user interface of an annotation tool for drawing on mobile devices. The tool is not finished yet.

![Screenshot UI](Screenshot-UI.png)

## Prerequisites

- Node >= v16.8.0

## Input Data

Images from unsplash were used in the project. These are randomly chosen and pure sample images. The code was written in such a way that both the images and the description of the task what is to be marked, by adjustment of the variables `description` and `images` can be simply exchanged or dynamically loaded for example from a server.
